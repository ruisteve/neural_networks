#!/usr/bin/perl
use warnings;
use strict;
use v5.18;

use Time::HiRes 'gettimeofday','tv_interval';
use Math::Lapack::Expr;
use Math::Lapack::Matrix;
use AI::NeuralNetwork 'softmax';
use Data::Dumper;

sub accuracy {
    my ($y, $prob) = @_;
    my ($rows, $cols) = $y->shape;
    my $tot = 0;
    for my $j (0..$cols-1){
        my $max = $prob->get_element(0,$j);
        my $pos_x = 0;
        my $pos_y = $j;
        for my $i (0..9){
            if ($prob->get_element($i,$j) > $max) {
                $pos_x = $i;
                $max = $prob->get_element($i,$j);
            }
        }
        $tot++ if $y->get_element($pos_x, $pos_y) == 1;
    }
    return $tot/$cols;
}        

sub print_line_transpose {
    my ($y) = @_;
    my ($rows, $cols) = $y->shape;
    for my $j (0..5){
        for my $i (0..$rows){
            my $val = $y->get_element($i,$j);
            print STDERR "$val\t";
        }
        print STDERR "\n";            
    }
}

sub print_line {
    my ($y) = @_;
    my ($rows, $cols) = $y->shape;
    for my $i (0..10){
        for my $j (0..$cols){
            my $val = $y->get_element($i,$j);
            print STDERR "$val\t";
        }
        print STDERR "\n";            
    }
}
sub get_loss {
    my ($prob, $y, $w, $lam) = @_;
    my $m = $y->columns; 
    my $result = (-1/$m) * sum($y * $prob->log) + $lam * sum($w * $w);
    return $result->get_element(0,0);
}

sub get_grad {
    my ($x, $y, $prob, $lam, $w) = @_;
    my $m = $y->columns;
    my $aux = ($y - $prob) x $x->transpose;
    my $grad = (-1/$m)* $aux + $lam * $w;
    return $grad;
}

sub print_probs {
    my ($prob) = @_;
    for(0..10){
        my $value = $prob->get_element($_,1);
        print "$value\n";
    }
} 

my $X = Math::Lapack::Matrix->read_matrix("train_images.txt")->transpose;
$X = $X / 255;
my ($nx, $m) = $X->shape;
print "X feature_count=$nx example_count=$m\n";
my $Y = Math::Lapack::Matrix->read_matrix("train_labels.txt")->transpose;
print_line_transpose $Y;
my ($y_0, $y_1) = $Y->shape;
print "Y shape $y_0,$y_1\n";
my $w = Math::Lapack::Matrix->zeros(10,$nx);


my $lam = 1 / (2*$m); #not used yet
my $learning_rate = 0.7;
my $nr_iters = 400;
my $probs;
my $t0 = [gettimeofday];
for my $iter (1..$nr_iters) {
    $probs = softmax($w x $X);
    my $loss = get_loss($probs, $Y, $w, $lam);
    print STDERR "Loss[$iter] -> $loss\n" unless $iter % 10;
    my $grad = get_grad($X, $Y, $probs, $lam, $w);
    $w = $w - ($learning_rate * $grad);
    my $ac = accuracy($Y, $probs);
    print STDERR "Accuracy: $ac\n" unless $iter % 10;
}
my $total = tv_interval($t0);
printf("Perl execution time %.8f\n", $total);
##printf("%d\n",prob($A2, $Y))e
