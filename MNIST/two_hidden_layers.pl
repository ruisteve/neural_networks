#!/usr/bin/perl -w
use strict;

use Math::Lapack::Matrix;


sub prediction {
	my ($x, $y, $rows, $cols) = @_;
 	my $right = 0;
  	foreach my $j (0 .. 10){
    		my $value = 0;
    		my $prob = $x->get_element(0,$j);
		printf("%d -> %f\n", $value, $prob);
      		foreach my $i (1 .. $rows - 1){
      			my $aux = $x->get_element($i, $j);
			if($aux > $prob){
        			$value = $i;
        			$prob = $aux;
      			}
			printf("%d -> %f\n", $i, $aux);
    		}
		printf("\n\n\n");
    		$right++ if $y->get_element($j,0) == $value;
  	}
  	print $right;
  	return ($right/$cols);
}

sub print_x {
  my $x = shift;
  for my $i (0 .. 5) {
    for my $j (0 .. 5) {
      printf("%f\n", $x->get_element($i,$j));
    }
  }
}

sub print_y {
  my $y = shift;
  my ($rows, $cols) = $y->shape;
  for my $i (0 .. $rows){
    printf("%f\n", $y->get_element(0,$i));
  }
}

sub print_yatt {
  	my $y = shift;
	my ($rows, $cols) = $y->shape;
  	for my $i (0 .. $rows-1 ) {
      for my $j (0 .. 0) { #$cols-1 ) {
        printf("%f ", $y->get_element($i,$j)); 
      }
      printf("\n");
  	}
}

sub count{
	my ($m, $value) = @_;
	my ($rows, $cols) = $m->shape;
	my $v;
	my $n = 0;
  	for my $i (0 .. $rows - 1 ) {
    		for my $j (0 .. $cols - 1 ) {
			$v = $m->get_element($i,$j); 
      			$n++ if $v != $value;
    		}
  	}
	printf("Non %f = %d\n", $value, $n);
}



my $X = Math::Lapack::Matrix->read_matrix("train_images.txt")->transpose;
$X = $X / 255;
my ($nx, $m) = $X->shape;
my $Y = Math::Lapack::Matrix->read_matrix("train_labels.txt")->transpose;

my $nh = 2 * $nx;
my $ny = 10;

my $w1 = Math::Lapack::Matrix->random($nh, $nx) * 0.01;
my $w2 = Math::Lapack::Matrix->random($nh, $nh) * 0.01;
my $w3 = Math::Lapack::Matrix->random($ny, $nh) * 0.01;

my $b1 = Math::Lapack::Matrix->zeros($nh, 1);
my $b2 = Math::Lapack::Matrix->zeros($nh, 1);
my $b3 = Math::Lapack::Matrix->zeros($ny, 1);

my $learning_rate = 0.1;

my $A2;

for (1..20) {
  print STDERR "Starting iter $_\n";
  print "= Iter $_\n";

  my $Z1 = $w1 x $X + $b1;  # (nh,nx) x (nx, m) = (nh, m)
  my $A1 = sigmoid($Z1);
print STDERR ".";
  my $Z2 = $w2 x $A1 + $b2; # (nh, nh) x (nh, m) = (nh, m)
  my $A2 = sigmoid($Z2);
print STDERR ".";
  my $Z3 = $w3 x $A2 + $b3;# (ny, nh) x (nh, m) = (ny, m)
  my $A3 = softmax($Z3);
print STDERR "\n";
  print_yatt($A3);
  ## -------------

  my $dZ3 = $A3 - $Y; # (ny, m) - (ny, m) = (ny,m)
  my $dw3 = (1/$m) * ($dZ3 x transpose($A2)); # (ny,m) * (m, nh) = (ny, nh)
  my $db3 = (1/$m) * sum($dZ3,0); # (ny, 1)

  my $dZ2 = (transpose($w3) x $dZ3) * d_sigmoid($A2); # (nh, ny) * (ny, m) element-wise (nh,m) = (nh,m)
  my $dw2 = (1/$m) * ($dZ2 x transpose($A1)); #(nh,m) * (m, nx) = (nh, nx)
  my $db2 = (1/$m) * sum($dZ2,0);

  my $dZ1 = (transpose($w2) x $dZ2) * d_sigmoid($A1); # (nh, ny) * (ny, m) element-wise (nh,m) = (nh,m)
  my $dw1 = (1/$m) * ($dZ1 x transpose($X)); #(nh,m) * (m, nx) = (nh, nx)
  my $db1 = (1/$m) * sum($dZ1,0);

  
  ## ------------

  $w1 = $w1 - $learning_rate * $dw1;
  $b1 = $b1 - $learning_rate * $db1;

  $w2 = $w2 - $learning_rate * $dw2;
  $b2 = $b2 - $learning_rate * $db2;

  $w3 = $w3 - $learning_rate * $dw3;
  $b3 = $b3 - $learning_rate * $db3;

}
