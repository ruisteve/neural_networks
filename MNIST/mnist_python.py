import mnist
import numpy as np

def sigmoid(x):
	return 1 / (1 + np.exp(-x))

def softmax(x):
	return np.exp(x) / np.sum(np.exp(x), axis=0) #check right axis

def d_sigmoid(x):
	f = sigmoid(x)
	return f * (1 -f)

def label(label):
	Y = np.zeros((10, 60000))
	for i in range(0,60000):
		Y[label[i],i] = 1
	return Y
	
		
x_train, t_train, x_test, t_test = mnist.load()

Y = label(t_train)
X = np.transpose(x_train) / 255
nx, m = X.shape

nh = 2 * nx
ny = 10

w1 = np.random.rand(nh, nx) * .01
w2 = np.random.rand(ny, nh) * .01

b1 = np.zeros((nh, 1))
b2 = np.zeros((ny, 1))

learning_rate = 0.5

for i in range(0,6):
	print(i)
	
	Z1 = np.dot(w1, X) + b1

	A1 = sigmoid(Z1)

	Z2 = np.dot(w2, A1) + b2

	A2 = softmax(Z2)

	#-----back prop
	
	dZ2 = A2 - Y
	dw2 = (1/m) * np.dot(dZ2, np.transpose(A1))
	db2 = (1/m) * np.sum(dZ2, axis = 0)
	
	dZ1 = np.dot(np.transpose(w2), dZ2)
	dw1 = (1/m) * np.dot(dZ1, np.transpose(X))
	db1 = (1/m) * np.sum(dZ1, axis=0)

	#---
	w1 = w1 - learning_rate * dw1
	b1 = b1 - learning_rate * db1

	w2 = w2 - learning_rate * dw2
	b2 = b2 - learning_rate * db2

print(A2[:,0])
