#!/usr/bin/python

import numpy as np
import mnist


def reshape_Y(y):
    y = y.reshape(60000,1)
    rows, cols = y.shape
    yatt = np.zeros((rows,10))
    for i in range(0,rows):
        yatt[i,y[i,0]] = 1
    return yatt

def relu(x):
    return x * (x > 0)

def d_relu(x):
    x[x<=0]= 0
    x[x>0] = 1
    return x


def get_max(x):
    return np.amax(x)

def softmax(x):
    a = np.exp( x - get_max(x) )
    vm = np.sum(x, axis = 0, keepdims = True)
    return a / vm


def accuracy(y, prob):
    rows, cols = y.shape
    tot = 0

    for j in range(0, cols):
        bigger = prob[0, j]
        pos_x = 0
        pos_y = j
        for i in range(0, rows):
            val = prob[i,j]
            if val > bigger:
                pos_x = i
                bigger = val
        if y[pos_x, pos_y] == 1:
            tot+=1
    return tot/cols

x_train, t_train, x_test, t_test = mnist.load()

X = np.transpose(x_train) / 255
Y = np.transpose(reshape_Y(t_train))
feature,m = X.shape

w1 = np.random.rand(1000, 784) * .01
w2 = np.random.rand(10, 1000) * .01

b1 = np.zeros((1000,1))
b2 = np.zeros((10,1))

learning_rate = 0.1

for i in range(0,2):
    print(i)

    Z1 = np.dot(w1, X) + b1
    A1 = relu(Z1)

    Z2 = np.dot(w2, A1) + b2
    A2 = softmax(Z2)

    ac = accuracy(Y, A2)
    print(ac)
    
    dZ2 = A2 - Y
    print("dZ2",dZ2.shape)
    dw2 = (1/m) * np.dot(dZ2, A1.T)
    print("dw2", dw2.shape)
    db2 = (1/m) * np.sum(dZ2, axis=1, keepdims = True)
    print("db2", db2.shape)
    dA1 = np.dot(w2.T, dZ2)
    print("dA1", dA1.shape)
    print("d_relu", d_relu(Z1).shape)
    dZ1 = np.dot(dA1, d_relu(Z1))
    dw1 = (1/m) * np.dot(dZ1, X.T)
    db1 = (1/m) * np.sum(dZ1, axis = 1, keepdims = True)

    w1 = w1 - learning_rate * dw1
    w2 = w2 - learning_rate * dw2

    b1 = b1 - learning_rate * db1
    b2 = b2 - learning_rate * db2


