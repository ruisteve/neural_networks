#!/usr/bin/perl
use warnings;
use strict;
<<<<<<< HEAD
use v5.18;

use Math::Lapack::Expr;
=======
use Time::HiRes 'gettimeofday','tv_interval';
>>>>>>> b1cec41aab1ab7b0694537f808a824edbfc45f6d
use Math::Lapack::Matrix;
use AI::NeuralNetwork;

sub accuracy {
    my ($y, $prob) = @_;
    my ($rows, $cols) = $y->shape;
    my $tot = 0;
    for my $j (0..$cols-1){
        my $max = $prob->get_element(0,$j);
        my $pos_x = 0;
        my $pos_y = $j;
        for my $i (1..9){
            my $val = $prob->get_element($i,$j);
            if ($val > $max) {
                $pos_x = $i;
                $max = $val;
            }
        }
        $tot++ if $y->get_element($pos_x, $pos_y) == 1;
    }
    return $tot/$cols;
}

sub shape($$) {
    my ($name, $x) = @_;
    my @s = $x->shape();
    print STDERR "$name is @s\n";
}


my $X = Math::Lapack::Matrix->read_matrix("train_images.txt")->transpose / 255;
my $Y = Math::Lapack::Matrix->read_matrix("train_labels.txt")->transpose;

<<<<<<< HEAD
my $w1 = Math::Lapack::Matrix->random(1000, 784) * .01;
my $w2 = Math::Lapack::Matrix->random(10, 1000) * .01;

my $b1 = Math::Lapack::Matrix->zeros(1000, 1);
my $b2 = Math::Lapack::Matrix->zeros(10, 1);

my (undef, $m) = $X->shape();
my $learning_rate = 0.1;

#shape "X", $X;
#shape "Y", $Y;
#shape "W1", $w1;
#shape "W2", $w2;
#shape "b1", $b1;
#shape "b2", $b2;

for my $iter (1..2) {
    print STDERR "iter: $iter\n";

    shape "-w1", $w1;
    shape "-X", $X;
    shape "-b", $b1;
    my $Z1 = $w1 x $X + $b1;
    shape "Z1", $Z1;
    my $A1 = AI::NeuralNetwork::relu($Z1);
    shape "A1", $A1;

    my $Z2 = $w2 x $A1 + $b2;
    shape "Z2", $Z2;
    my $A2 = AI::NeuralNetwork::softmax($Z2);
    shape "A2", $A2;
 
    my $ac = accuracy($Y, $A2);
    print STDERR "Accuracy: $ac\n";

    my $dZ2 = $A2 - $Y;
    shape "dZ2", $dZ2;
    my $dW2 = (1/$m) * $dZ2 x $A1->transpose;
    shape "dW2", $dW2;
    my $db2 = (1/$m) * sum($dZ2, 0);
    shape "db2", $db2;
    my $dA1 = $w2->transpose x $dZ2;
    shape "dA1", $dA1;

    my $dZ1 = $dA1 x AI::NeuralNetwork::d_relu($Z1);
    shape "dZ1", $dZ1;
    my $dW1 = (1/$m) * $dZ1 x $X->transpose;
    shape "dW1", $dW1;
    my $db1 = (1/$m) * sum($dZ1, 0);
    shape "db1", $db1;

    $w1 = $w1 - $learning_rate * $dW1; 
    shape "W1", $w1;
    $w2 = $w2 - $learning_rate * $dW2; 
    shape "W2", $w2;

    $b1 = $b1 - $learning_rate * $db1;
    shape "b1", $b1;
    $b2 = $b2 - $learning_rate * $db2;
    shape "b2", $b2;

}
=======
my $nh = 2 * $nx;
my $ny = 10;

my $w1 = Math::Lapack::Matrix->random($nh, $nx) * 0.01;
my $w2 = Math::Lapack::Matrix->random($ny, $nh) * 0.01;

my $b1 = Math::Lapack::Matrix->zeros($nh, 1);
my $b2 = Math::Lapack::Matrix->zeros($ny, 1);

my $learning_rate = 0.35;

my $A2;

my $nr_iters = 1;
my $t0 = [gettimeofday];
for (1..$nr_iters) {
  print STDERR "Starting iter $_\n";
  print "= Iter $_\n";

  my $Z1 = $w1 x $X + $b1;  # (nh,nx) x (nx, m) = (nh, m)

#  print "== Z1\n";
#  print_yatt($Z1);

  my $A1 = sigmoid($Z1);
#  print "== A1\n";
#  print_yatt($A1);


  my $Z2 = $w2 x $A1 + $b2; # (ny, nh) x (nh, m) = (ny, m)
#  print "== Z2\n";
  print_yatt($Z2);

  $A2 = softmax($Z2);
#  print "== A2\n";
  print_yatt($A2); 
  #print_right_value($X, 0);
  ## -------------

  my $dZ2 = $A2 - $Y; # (ny, m) - (ny, m) = (ny,m)
  my $dw2 = (1/$m) * ($dZ2 x transpose($A1)); # (ny,m) * (m, nh) = (ny, nh)
  my $db2 = (1/$m) * sum($dZ2,0); # (ny, 1)

  my $dZ1 = (transpose($w2) x $dZ2) * d_sigmoid($A1); # (nh, ny) * (ny, m) element-wise (nh,m) = (nh,m)
  my $dw1 = (1/$m) * ($dZ1 x transpose($X)); #(nh,m) * (m, nx) = (nh, nx)
  my $db1 = (1/$m) * sum($dZ1,0);

  ## ------------

  $w1 = $w1 - $learning_rate * $dw1;
  $b1 = $b1 - $learning_rate * $db1;

  $w2 = $w2 - $learning_rate * $dw2;
  $b2 = $b2 - $learning_rate * $db2;


}
printf("Perl time of execution %.8f", tv_interval($t0));
#printf("%d\n",prob($A2, $Y));
>>>>>>> b1cec41aab1ab7b0694537f808a824edbfc45f6d
