# ABSTRACT: Perl interface to LAPACK
use strict;
use warnings;
package Math::Lapack;

use parent 'DynaLoader';

bootstrap Math::Lapack;
sub dl_load_flags { 1 }

=pod

=for Pod::Coverage seed_rng

=cut


=head2 DESCRIPTION

This module exists, for now, as a wrapper for the C/XS code, for interaction with Lapack C libraries.
Please refer to L<Math::Lapack::Matrix> for usage details.

=cut

sub seed_rng {
  my $val = shift;
  $val = shift if $val eq __PACKAGE__;
  _srand($val);
}

1;
