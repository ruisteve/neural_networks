#!/usr/bin/env python3

import numpy as np
import LinearRegression as LR
import LogisticRegression as Logistic
from sklearn.preprocessing import StandardScaler,MinMaxScaler
from sklearn.metrics import accuracy_score,f1_score,precision_score,recall_score
from sklearn.linear_model import LogisticRegression


def load_data_logistic():
    data = np.loadtxt(open("logistic.csv", "rb"), delimiter=",")
    print(data.shape)
    x = data[:,:3]
    y = data[:,3:]
    y-=1
    return (x,y)


def aux_test():
    x,y = load_data_logistic()
    logreg = LogisticRegression(solver='saga')
    scaler = StandardScaler()
    x = scaler.fit_transform(x)
    #print(y)
    #print(y.ravel())
    logreg.fit(x,y.ravel())
    y_pred = logreg.predict(x)
    acc = accuracy_score(y,y_pred)
    prec = precision_score(y,y_pred)
    recall = recall_score(y,y_pred)
    f1 = f1_score(y,y_pred)
    print(acc,prec,recall,f1)



def train_logistic_regression():
    x,y = load_data_logistic()
    #print(x)
    sc = MinMaxScaler()
    #x = sc.fit_transform(x) same results with or without normalization
    #print(x) 
    scaler = StandardScaler()
    x_norm = scaler.fit_transform(x)
    #print(x_norm)
    #print("Without regularization")
    t = np.array([
        [0.840187728404999],
        [0.394382923841476],
        [0.783099234104156],
        [0.798440039157867]
        ])
    thetas = Logistic.train(x_norm,y,t, 10000, 0.5)
    print(thetas)
    pred = Logistic.prediction(x_norm,thetas)
    acc = accuracy_score(y,pred)
    prec = precision_score(y,pred)
    recall = recall_score(y,pred)
    f1 = f1_score(y,pred)
    print(acc,prec,recall,f1)


def train_logistic_regression_reg():
    x,y = load_data_logistic()
    scaler = StandardScaler()
    x_norm = scaler.fit_transform(x)
    t = np.array([
        [0.911647379398346],
        [0.197551369667053],
        [0.335222750902176],
        [0.768229603767395]
        ])
    thetas = Logistic.train(x_norm,y,t, 10000, 0.5,2)
    print(thetas)
    pred = Logistic.prediction(x_norm,thetas)
    acc = accuracy_score(y,pred)
    prec = precision_score(y,pred)
    recall = recall_score(y,pred)
    f1 = f1_score(y,pred)
    print(acc,prec,recall,f1)



def main():
    a = np.array(
            [
                [0, 0.002, 3, 4, 7.333, -.00008, -2.03456, 9, 100.3456, -300]
            ]
            )

    x = np.array(
            [[12.39999962],[14.30000019],[14.5],[14.89999962],[16.10000038],[16.89999962],[16.5],[15.39999962],[17],[17.89999962],[18.79999924],[20.29999924],[22.39999962],[19.39999962],[15.5],[16.70000076],[17.29999924],[18.39999962],[19.20000076],[17.39999962],[19.5],[19.70000076],[21.20000076]]
            )


    y = np.array(
            [[11.19999981],[12.5],[12.69999981],[13.10000038],[14.10000038],[14.80000019],[14.39999962],[13.39999962],[14.89999962],[15.60000038],[16.39999962],[17.70000076],[19.60000038],[16.89999962],[14],[14.60000038],[15.10000038],[16.10000038],[16.79999924],[15.19999981],[17],[17.20000076],[18.60000038]]
            )
    aux_test()
    #train_logistic_regression_reg()
    #print(LR.train(x,y,50,.001,False,1))

    #print(a.shape)
    #print("Hello")

if __name__ == "__main__":
    main()
