#!/usr/bin/env python3

import numpy as np
import Expr as expr

def train(x, y, thetas,iters, alpha = 0.01, l=0):
    m,n = x.shape
    x = np.append(np.ones((m,1)),x,axis = 1) #append vertically
    n += 1
    
    if l == 0:
        for i in range(1,iters+1):
            h = expr.sigmoid( np.dot(x, thetas) )
            if not i % 10:
                cost = (-1/m)*np.sum((y*np.log(h))+((1-y)*np.log(1-h)))
                #print(cost)
            grads = np.dot( x.T, (h - y)) / m
            thetas = thetas - alpha * grads
    else:
        for i in range(1,iters+1):
            h = expr.sigmoid( np.dot(x,thetas ))
            if not i % 10:
                reg = (l/(2*m))*np.sum(thetas[1:]**2)
                cost = (-1/m)*np.sum((y*np.log(h))+((1-y)*np.log(1-h)))+reg
                #print(cost)
            reg_thetas = ( l/m ) * thetas
            reg_thetas[0,0] = 0
            grads = np.dot( x.T, (h - y)) / m
            thetas = thetas - alpha * (grads + reg_thetas)
    return thetas


def prediction(x,thetas):
    m,n = x.shape
    x = np.append(np.ones((m,1)),x,axis = 1) #append vertically
    h = expr.sigmoid( np.dot(x, thetas) )
    #print(h)
    return np.where( h >= 0.5, 1, 0 )
