#!/usr/bin/env python3

import numpy as np

def train(x, y, iters, alpha = 0.01, normal=False, l=0):
    m,n = x.shape
    x = np.append(np.ones((m,1)),x,axis = 1) #append vertically
    n += 1
    
    if normal:
        return np.dot( np.dot(np.linalg.inv(np.dot(x.T,x)) ,x.T ) , y )
    else:
        if l == 0:
            thetas = np.array(
            [
                [0.840187728404999],
                [0.394382923841476]
            ]
            )
            for i in range(1,iters+1):
                if not i % 10:
                    cost = np.sum((np.dot(x,thetas)-y)**2) / (2 * m)
                    print(cost)
                grads = np.dot( x.T, (np.dot(x,thetas)-y)) / m
                thetas = thetas - alpha * grads
        else:
            thetas = np.array([
                [0.783099234104156],
                [0.798440039157867]
            ])
            print("inside")
            for i in range(1,iters+1):
                if not i % 10:
                    cost = np.sum((np.dot(x,thetas)-y)**2) / (2 * m) + l * np.sum(thetas[1:])
                    print(cost)
                grads = np.dot( x.T, (np.dot(x,thetas)-y)) / m
                reg_thetas = ( l/m ) * thetas
                reg_thetas[0,0] = 0
                thetas = thetas - alpha * (grads + reg_thetas)

        return thetas

