#!/usr/bin/env python3
import numpy as np

def sigmoid(x, derivative=False):
    return sigmoid(x)*(1-sigmoid(x)) if derivative else 1/(1+np.exp(-x))


def tanh(x, derivative=False):
    return 1 - (np.tanh(x)** 2) if derivative else np.tanh(x)


def relu(x, derivative=False):
    if derivative:
        b = np.array(x, copy=True)
        b[x < 0] = 0
        b[x >= 0] = 1
        return b
    else:
        return np.maximum(0,x)


def lrelu(x, v, derivative = False):
    b = np.array(x, copy = True)
    if derivative:
        b = np.where( x >= 0, 1, v )
    else:
        b = np.where( x >= 0, x, x*v)
    return b

def softmax(x):
    e_x = np.exp( x - np.max(x) )
    return e_x / e_x.sum()

