#!/usr/bin/env perl

use warnings;
use strict;

my ($file1, $file2) = @ARGV;

open FILE1, "<", $file1 or die "$!";
open FILE2, "<", $file2 or die "$!";

while (!eof(FILE1) && !eof(FILE2)){
    my $l1 = <FILE1>; chomp $l1;
    my $l2 = <FILE2>; chomp $l2;  
    my @a = split(/,/, $l1);
    my @b = split(/,/, $l2);

    my ($min, $max);
    
    for (my $i = 0; $i < scalar(@a); $i++){
        $min = $a[$i] - .001;
        $max = $a[$i] + .001;
        unless( $b[$i] >= $min && $b[$i] <= $max ){
            print "Big margin between $a[$i] ---- $b[$i]\n";
        }
    }
}
