#include <stdio.h>
#include <stdlib.h>
#include "header.h"
#include <assert.h>
#define NEW_MATRIX(m,r,c)   m=(Matrix*)malloc(sizeof(Matrix));\
                            m->rows = r; m->columns = c;\
                            m->values = (float*) malloc (r * c * sizeof(float));

// TODO:
//  - Check if malloc succeeded


Matrix *dirty(int rows, int columns) {
    Matrix *M;
    NEW_MATRIX(M, rows, columns)    
    return M;
}

Matrix *zeros(int rows, int columns) {
    int i;
    Matrix *M;

	if(!(rows > 0 && columns > 0))
		exit(1);

    NEW_MATRIX(M, rows, columns)	

	for(i = 0; i < rows * columns; i++)
		M->values[i] = 0;
    return M;
}

Matrix *ones(int rows, int columns) {
    int i;
	Matrix *M;

	if(!(rows > 0 && columns > 0))
		exit(1);
	
    NEW_MATRIX(M, rows, columns)	
    
	for(i = 0; i < rows * columns; i++)
		M->values[i] = 1;

	return M;
}

Matrix *matrix_random(int rows, int columns) {
	int i;
	Matrix *M;

    NEW_MATRIX(M, rows, columns)	
	
	for(i = 0; i < rows * columns; i++)
		M->values[i] = (float)rand() /  RAND_MAX;
	
	return M;
}


Matrix *identity(int rows) {
	int i;
	Matrix *M = zeros(rows,rows);
	
	for(i = 0; i < rows * rows; i+= rows + 1)
		M->values[i] = 1;
	
	return M;
}


Matrix *multiply(Matrix *A, Matrix *B) {
	Matrix *C;
	char trans = 'N';
	float alpha = 1, beta = 0;

	if (A->columns != B->rows) exit(1);

	NEW_MATRIX(C, A->rows, B->columns);
	int m = A->rows;
	int n = B->columns;
	int k = A->columns;


	sgemm_(&trans, &trans, &n, &m, &k, &alpha, B->values, &n, A->values, &k, &beta, C->values, &n);
	return C;
} 


void show_matrix(Matrix *M) {
	int i, j, r, c;
	double v;

	r = M->rows;
	c = M->columns;
	
	for(i = 0; i < c; i++)
		for( j = 0; j < r; j++)
			printf("M(%d,%d) = %.2f\n", i, j , M->values[i * r + j]);
}

	

float get_element(Matrix *m, int i, int j) {
   	if(!(i > -1 && j > -1)) exit(1);
	return m->values[ i * m->columns + j ];
}

void set_element(Matrix *m, int i, int j, float v) {
	if(!(i > -1 && j > -1)) exit(1); 
    m->values[ i * m->columns + j ] = v;
}
	

Matrix *element_wise(Matrix *m, float f(float)) {
    Matrix *new = zeros(m->rows, m->columns);
    int i, j;
    for (i = 0; i < m->rows * m->columns; i++)
        new->values[i] = f(m->values[i]);
    return new;
}

Matrix *sum_matrices(Matrix *A, Matrix *B){
	Matrix *C;
	int i, rows, cols;
	if(!(A->rows == B->rows && A->columns == B->columns)) 
		exit(1);

	rows = A->rows;
	cols = A->columns;
	
	C = zeros(rows, cols);
	
	for(i = 0; i < rows * cols; i++)
		C->values[i] = A->values[i] + B->values[i];

	return C;
}

Matrix *sub_matrices(Matrix *A, Matrix *B){
	Matrix *C;
	int i, rows, cols;
	if(!(A->rows == B->rows && A->columns == B->columns)) 
		exit(1);

	rows = A->rows;
	cols = A->columns;
	
	C = zeros(rows, cols);
	
	for(i = 0; i < rows * cols; i++)
		C->values[i] = A->values[i] - B->values[i];

	return C;
}

void destroy(Matrix *m){
	free(m->values);
	free(m);
}

Matrix *transpose(Matrix *m){
        int rows, cols, i, j;
        Matrix *t;
        rows = m->rows;
        cols = m->columns;

        NEW_MATRIX(t, cols, rows);

        for( i = 0; i < rows; i++)
                for( j = 0; j < cols; j++)
                        t->values[i * cols + j] = m->values[j * rows + i];

        return t;
}

Matrix *inverse(Matrix *m){
        int j, n, info;
        Matrix *i;
        n = m->rows;
        int ipiv[n];
        float work[n * n];

        NEW_MATRIX(i, m->rows, m->columns);

        for(j = 0; j < m->rows * m->columns; j++)
                i->values[j] = m->values[j];

        dgetrf_(&n, &n, i->values, &n, ipiv, &info);

        if(info != 0) exit(1);

	dgetri_(&n, i->values, &n, ipiv, work, &n, &info);

        if(info != 0) exit (1);

        return i;
}

void endianSwap(unsigned int *x) {
  *x = (*x>>24)|((*x<<8)&0x00FF0000)|((*x>>8)&0x0000FF00)|(*x<<24);
}

Matrix *read_csv(char *path){
	FILE *fimage = fopen(path, "rb");
	unsigned int magic, num, row, col;
	if(fread(&magic, 4, 1, fimage) <= 0) exit(1);
	assert(magic == 0x03080000);
	if(fread(&num, 4, 1, fimage) <= 0) exit(1); 
	endianSwap(&num);
	if(fread(&row, 4, 1, fimage) <= 0) exit(1); 
	endianSwap(&row);
	if(fread(&col, 4, 1, fimage) <= 0) exit(1); 
	endianSwap(&col);

	Matrix *matrices = (Matrix*)malloc(num * col * row * sizeof(Matrix));
	
	int tot = 0,i, size = row * col;
	NEW_MATRIX(matrices, num, size);
	for(i = 0; i < num * size; i++){
		if(fread(&matrices->values[i], 1, 1, fimage) <= 0){
			exit(1);
		}
	}
	fclose(fimage);
	return matrices;
}
/*
Matrix *read_label_csv(char *path){
	FILE *flabel = fopen(path, "rb");
	unsigned int magic, num;
	fread(&magic, 4, 1, flabel);
	assert(magic == 0x01080000);
	fread(&num, 4, 1, flabel);
	Matrix *label;
	NEW_MATRIX(label, num, 1);
	
	int i;
	for( i = 0; i < num; i++)
		fread(label->values[i], 1, 1, flabel);

	return label;
}*/	
