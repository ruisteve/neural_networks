import numpy as np;
import time;

count = 20
total = 0

for j in range(0, count):
    start_time = time.time()
    a = np.random.rand(10000,10000)
    c = np.sum(a, axis = 0)
    c = np.sum(a, axis = 1)
    total += (time.time() - start_time)

mean = total / count

print("Python execution time mean: %.8f sec\n" % mean)
