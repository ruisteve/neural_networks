#!/usr/bin/perl -w
use strict;
use Math::Lapack::Matrix;
use Time::HiRes;

my $start = Time::HiRes::time();
for (0..1000){
	Math::Lapack::Matrix->random(1000,1000);
}

my $duration = Time::HiRes::time() -$start;
print("Execution time $duration s\n");
