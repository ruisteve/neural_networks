import numpy as np

def sigmoid(x):
	return (1 / (1 + np.exp(-x)))


def cost_function(features, labels, weights):
    '''
    Using Mean Absolute Error

    Features:(100,3)
    Labels: (100,1)
    Weights:(3,1)
    Returns 1D matrix of predictions
    Cost = ( log(predictions) + (1-labels)*log(1-predictions) ) / len(labels)
    '''
    observations = len(labels)

    predictions = sigmoid(np.dot(features, weights))

    #Take the error when label=1
    class1_cost = -labels*np.log(predictions)

    #Take the error when label=0
    class2_cost = (1-labels)*np.log(1-predictions)

    #Take the sum of both costs
    cost = class1_cost - class2_cost

    #Take the average cost
    cost = cost.sum()/observations

    return cost

x = np.array( [[5, 2, 5], [2, 1, 0], [3, 7, 9], [1, 0, 5], [1, 8, 3]] )
y = np.array( [[1],[0],[0],[0],[1]])
weights = np.random.rand(3,1)
print(weights)
print(cost_function(x, y, weights))

