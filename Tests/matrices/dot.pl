#!/usr/bin/perl

use strict;
use warnings;
use Math::Lapack::Matrix;
use Time::HiRes 'gettimeofday','tv_interval';

my $total = 0;
my $count = 20;

for (1..$count) {
    my $t0 = [gettimeofday];

    for (1..2_000) {
	   my $a = Math::Lapack::Matrix->random(105,100);
	   my $b = Math::Lapack::Matrix->random(100,105);
	   my $m = $a x $b;
    }

    $total += tv_interval($t0);
}
printf("Perl execution time mean: %.8f sec\n", $total / $count);
