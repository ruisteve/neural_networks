#!/usr/bin/perl

use strict;
use warnings;
use Math::Lapack::Matrix;
use Time::HiRes 'gettimeofday','tv_interval';

my $total = 0;
my $count = 20;
my $m;
for (1..$count) {
    my $t0 = [gettimeofday];

    my $a = Math::Lapack::Matrix->random(5000,10000);
    $m = transpose($a);

    $total += tv_interval($t0);
}
printf("Perl execution time mean: %.8f sec\n", $total / $count);
my ($rows, $cols) = $m->shape();
printf("Error") if $cols != 5000 || $rows != 10000; 
