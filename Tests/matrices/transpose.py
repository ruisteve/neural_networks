import numpy as np;
import time;

count = 20
total = 0

for j in range(0, count):
    start_time = time.time()
    a = np.random.rand(5000,10000)
    c = np.transpose(a)
    total += (time.time() - start_time)

mean = total / count

print("Python execution time mean: %.8f sec\n" % mean)

rows,cols = c.shape
if rows != 10000 or cols != 5000:
	print("Error")
