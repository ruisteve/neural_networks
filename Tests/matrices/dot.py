import numpy as np;
import time;

count = 20
total = 0

for j in range(0, count):
    start_time = time.time()
    for i in range(0, 2000):
        a = np.random.rand(105,100)
        b = np.random.rand(100,105)
        c = np.dot(a , b)
        
    total += (time.time() - start_time)

mean = total / count

print("Python execution time mean: %.8f sec\n" % mean)

