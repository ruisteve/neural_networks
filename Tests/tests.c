#include <sys/timeb.h>
#include <stdio.h> 
#include "header.h"
#define size 3				/* dimension of matrix */

#define SET(matrix, w, i, j, v)   matrix[i+j*w] = v


#if 0
int test1()
{
	int j , c1, c2, pivot[size], ok;
	float A[size*size], b[size];	/* single precision!!! */


	SET(A, size, 0, 0, 3.1);  SET(A, size, 0, 1, 1.3);  SET(A, size, 0, 2, -5.7);	/* matrix A */
	SET(A, size, 1, 0, 1.0);  SET(A, size, 1, 1, -6.9); SET(A, size, 1, 2, 5.8);	
	SET(A, size, 2, 0, 3.4);  SET(A, size, 2, 1, 7.2);  SET(A, size, 2, 2, -8.8);	

	b[0]=-1.3;			/* if you define b as a matrix then you */
	b[1]=-0.1;			/* can solve multiple equations with */
	b[2]=1.8;			/* the same A but different b */ 	
 
	c1=size;			/* and put all numbers we want to pass */
	c2=1;    			/* to the routine in variables */

	/* find solution using LAPACK routine SGESV, all the arguments have to */
	/* be pointers and you have to add an underscore to the routine name */
	sgesv_(&c1, &c2, A, &c1, pivot, b, &c1, &ok);      

	/*
 	parameters in the order as they appear in the function call
    	order of matrix A, number of right hand sides (b), matrix A,
    	leading dimension of A, array that records pivoting, 
    	result vector b on entry, x on exit, leading dimension of b
    	return value */ 
     						
	for (j=0; j<size; j++) printf("%e\n", b[j]);	/* print vector x */

return 0;
}
#endif


#if 0
void time_random_matrix(){
  	clock_t begin = time(NULL);
  	Matrix *M = matrix_random(10,10);
  
  	clock_t end = time(NULL);
  	double duration = (double)(end - begin);
  	printf("time :%f sec", duration);
	int i, j;
	double v;

}
#endif

/*
void mult_matrix_test(){
	Matrix *A = matrix_random(3,3);
	Matrix *B = matrix_random(3,3);
	Matrix *C;
	
	show_matrix(A);
	show_matrix(B);
	C = matrix_mult(A,B);
	show_matrix(C);

}
*/
int main_a(){
	int i, j;
    struct timeb begin, end;
	double duration;
	double mean = 0;
	Matrix *A;
	Matrix *B;
	Matrix *C;
	for(j = 0; j < 20; j++){
    		ftime(&begin);

		for(i = 0; i < 2000; i++){
			A = matrix_random(105,100);
			B = matrix_random(100,105);
			C = multiply(A, B);
		/*destroy(A);
		destroy(B);
		destroy(C);*/
		}

    		ftime(&end);
		mean += ((double)(end.millitm - begin.millitm)/1000) + (end.time - begin.time);
	}
	duration = mean / 20;
	printf("time :%f sec\n", duration);
	return 0;
}

int main_aux(){
	Matrix *A = matrix_random(4,4);
	Matrix *A_t = transpose(A);
	show_matrix(A);
	Matrix *B = inverse(A_t);
	Matrix *B_t = transpose(B);
	show_matrix(B_t);
	destroy(A);
	destroy(A_t);
	destroy(B);
	destroy(B_t);
	return 1;
}
		
int main(){
	char *path = "/home/rui/Documentos/datasets/train-images.idx3-ubyte";
	read_csv(path);
	return 0;
}
