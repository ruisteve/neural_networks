
typedef struct s_matrix {
	int columns;
	int rows;
	float *values;
} Matrix, *MatrixP;


void endianSwap(unsigned int *x);
Matrix *dirty(int rows, int columns);

Matrix *zeros(int rows, int columns);

Matrix *ones(int rows, int columns);

Matrix *matrix_random(int rows, int columns);

Matrix *identity(int rows);

Matrix *multiply(Matrix *A, Matrix *C);

void show_matrix(Matrix *M);

float get_element(Matrix*, int, int);
void set_element(Matrix*, int, int, float);

Matrix *sum_matrices(Matrix *A, Matrix *B);

Matrix *sub_matrices(Matrix *A, Matrix *B);
Matrix *transpose(Matrix *m);

Matrix *inverse(Matrix *m);

Matrix *read_label_csv(char *path);
Matrix *read_csv(char *path);

void dgetri_(int *, float *, int *, int *, float *, int *, int *);

void dgetrf_(int *, int *, float *, int *, int*, int*);
void destroy(Matrix *m);

void sgemm_   (char*, char*, int*, int*, int*, float*, float*, int*, float*, int*, float*, float*, int*);
