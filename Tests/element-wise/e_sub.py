import numpy as np;
import time;

count = 20
total = 0

for j in range(0, count):
    start_time = time.time()
    a = np.random.rand(10000,10000)
    c = a - 5
    c = 5 - a
    total += (time.time() - start_time)

mean = total / count

print("Python execution time mean: %.8f sec\n" % mean)
rows,cols = c.shape
if rows != 10000 and cols != 10000:
	print("Error")
