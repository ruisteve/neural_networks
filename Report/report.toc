\babel@toc {portuguese}{}
\contentsline {chapter}{Lista de Figuras}{7}{chapter*.4}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{9}{chapter.1}
\contentsline {chapter}{\numberline {2}Introdu\IeC {\c c}\IeC {\~a}o a Machine Learning}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Machine Learning}{11}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Exemplo de Machine Learning}{11}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Limita\IeC {\c c}\IeC {\~o}es de Machine Learning}{11}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Hist\IeC {\'o}ria de Machine Learning}{11}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Antes de 1940}{11}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Computing machinery and intelligence (1950)}{12}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Primeira rede neuronal (1951)}{12}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}O primeiro 'inverno' da IA (1980)}{12}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Deep Blue ganha Garry Kasparov (1996)}{12}{subsection.2.2.5}
\contentsline {subsection}{\numberline {2.2.6}S\IeC {\'e}culo XXI}{12}{subsection.2.2.6}
\contentsline {subsubsection}{Backpropagation (2006)}{12}{section*.5}
\contentsline {subsubsection}{GoogleBrain (2012)}{12}{section*.6}
\contentsline {subsubsection}{AlexNet (2012)}{13}{section*.7}
\contentsline {subsubsection}{DeepFace (2014)}{13}{section*.9}
\contentsline {subsubsection}{ResNet (2015)}{13}{section*.10}
\contentsline {subsubsection}{U-net (2015)}{14}{section*.12}
\contentsline {section}{\numberline {2.3}Import\IeC {\^a}ncia das GPUs}{15}{section.2.3}
\contentsline {chapter}{\numberline {3}Teoria de Machine Learning}{17}{chapter.3}
\contentsline {section}{\numberline {3.1}Aprendizagem Supervisionada}{17}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Nota\IeC {\c c}\IeC {\~o}es e conceitos gerais}{17}{subsection.3.1.1}
\contentsline {subsubsection}{Fun\IeC {\c c}\IeC {\~a}o Hip\IeC {\'o}tese}{17}{section*.16}
\contentsline {subsubsection}{Fun\IeC {\c c}\IeC {\~a}o de Perda}{17}{section*.17}
\contentsline {subsubsection}{Fun\IeC {\c c}\IeC {\~a}o de Custo}{18}{section*.19}
\contentsline {subsubsection}{Algoritmos de Otimiza\IeC {\c c}\IeC {\~a}o}{18}{section*.20}
\contentsline {paragraph}{Algoritmos de Primeira Ordem}{18}{section*.21}
\contentsline {paragraph}{Algoritmos de Segunda Ordem}{18}{section*.22}
\contentsline {paragraph}{Gradiente Descendente}{18}{section*.23}
\contentsline {subparagraph}{Stochastic Gradient Descent, SGD}{19}{section*.25}
\contentsline {subparagraph}{Mini Batch Gradient Descent}{19}{section*.26}
\contentsline {subparagraph}{Adam - \textit {Adam Moment Estimation} (\cite {adam})}{19}{section*.27}
\contentsline {section}{\numberline {3.2}Redes neuronais}{20}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Arquitetura}{20}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Fun\IeC {\c c}\IeC {\~o}es de Ativa\IeC {\c c}\IeC {\~a}o}{20}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Perda de entropia cruzada}{20}{subsection.3.2.3}
\contentsline {paragraph}{Fator de aprendizagem}{20}{section*.30}
\contentsline {subsection}{\numberline {3.2.4}Propaga\IeC {\c c}\IeC {\~a}o para Tr\IeC {\'a}s}{21}{subsection.3.2.4}
\contentsline {chapter}{\numberline {4}Ferramentas e Tecnologias}{23}{chapter.4}
\contentsline {section}{\numberline {4.1}Linguagens de Programa\IeC {\c c}\IeC {\~a}o}{23}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Perl}{23}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}C}{24}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}XS}{24}{subsection.4.1.3}
\contentsline {subsubsection}{C\IeC {\'o}digo em XS}{24}{section*.31}
\contentsline {section}{\numberline {4.2}Bibliotecas}{25}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}BLAS}{25}{subsection.4.2.1}
\contentsline {subsubsection}{Funcionalidades BLAS}{25}{section*.32}
\contentsline {paragraph}{Level 1}{25}{section*.33}
\contentsline {paragraph}{Level 2}{25}{section*.34}
\contentsline {paragraph}{Level 3}{26}{section*.35}
\contentsline {subsubsection}{Implementa\IeC {\c c}\IeC {\~o}es de BLAS}{26}{section*.36}
\contentsline {subsection}{\numberline {4.2.2}LAPACK}{26}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Ambiente de Desenvolvimento}{26}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Controlo de Vers\IeC {\~o}es}{26}{subsection.4.3.1}
\contentsline {subsubsection}{Git}{26}{section*.37}
\contentsline {subsection}{\numberline {4.3.2}Testes unit\IeC {\'a}rios}{26}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}POD (Plain Old Documentation)}{29}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}CPAN}{29}{subsection.4.3.4}
\contentsline {subsubsection}{CPAN Testers}{29}{section*.39}
\contentsline {chapter}{\numberline {5}Math-Lapack}{31}{chapter.5}
\contentsline {section}{\numberline {5.1}Math::Lapack::Matrix}{32}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Construtores}{32}{subsection.5.1.1}
\contentsline {subsubsection}{zeros}{32}{section*.41}
\contentsline {subsubsection}{uns}{33}{section*.42}
\contentsline {subsubsection}{random}{33}{section*.43}
\contentsline {subsubsection}{identity}{33}{section*.44}
\contentsline {subsubsection}{new}{33}{section*.45}
\contentsline {subsection}{\numberline {5.1.2}M\IeC {\'e}todos}{34}{subsection.5.1.2}
\contentsline {subsubsection}{get element}{34}{section*.46}
\contentsline {subsubsection}{set\_element}{34}{section*.47}
\contentsline {subsubsection}{rows}{34}{section*.48}
\contentsline {subsubsection}{columns}{35}{section*.49}
\contentsline {subsubsection}{max}{35}{section*.50}
\contentsline {subsubsection}{min}{35}{section*.51}
\contentsline {subsubsection}{reshape}{35}{section*.52}
\contentsline {subsubsection}{mean}{36}{section*.53}
\contentsline {subsubsection}{std deviation}{36}{section*.54}
\contentsline {subsubsection}{norm mean}{36}{section*.55}
\contentsline {subsubsection}{norm std deviation}{37}{section*.56}
\contentsline {subsection}{\numberline {5.1.3}Aritm\IeC {\'e}tica}{38}{subsection.5.1.3}
\contentsline {subsubsection}{broadcasting}{38}{section*.57}
\contentsline {subsubsection}{overloading}{40}{section*.58}
\contentsline {subsubsection}{Adi\IeC {\c c}\IeC {\~a}o}{41}{section*.59}
\contentsline {subsubsection}{Subtra\IeC {\c c}\IeC {\~a}o}{41}{section*.60}
\contentsline {subsubsection}{Divis\IeC {\~a}o}{41}{section*.61}
\contentsline {subsubsection}{Multiplica\IeC {\c c}\IeC {\~a}o}{42}{section*.62}
\contentsline {subsubsection}{Produto Interno}{42}{section*.63}
\contentsline {subsubsection}{Exponencial}{44}{section*.65}
\contentsline {subsubsection}{Pot\IeC {\^e}ncia}{45}{section*.66}
\contentsline {subsubsection}{Fun\IeC {\c c}\IeC {\~a}o Logar\IeC {\'\i }tmica}{45}{section*.67}
\contentsline {subsubsection}{Transposta}{46}{section*.68}
\contentsline {subsubsection}{Inversa}{46}{section*.69}
\contentsline {subsubsection}{Concatena\IeC {\c c}\IeC {\~a}o}{47}{section*.70}
\contentsline {subsubsection}{Append}{49}{section*.71}
\contentsline {subsubsection}{Matriz para lista}{50}{section*.72}
\contentsline {subsubsection}{sum}{50}{section*.73}
\contentsline {subsubsection}{slice}{51}{section*.74}
\contentsline {subsection}{\numberline {5.1.4}Leitura e Escrita de Matrizes}{53}{subsection.5.1.4}
\contentsline {subsubsection}{save csv}{53}{section*.75}
\contentsline {subsubsection}{read csv}{54}{section*.76}
\contentsline {subsubsection}{save}{54}{section*.77}
\contentsline {subsubsection}{read matrix}{55}{section*.78}
\contentsline {subsubsection}{save matlab}{55}{section*.79}
\contentsline {section}{\numberline {5.2}Math::Lapack::Expr}{56}{section.5.2}
\contentsline {subsubsection}{Linguagens de Dom\IeC {\'\i }nio Especifico}{56}{section*.80}
\contentsline {subsubsection}{Motiva\IeC {\c c}\IeC {\~a}o}{57}{section*.81}
\contentsline {subsection}{\numberline {5.2.1}Arquitetura}{57}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Cria\IeC {\c c}\IeC {\~a}o da AST}{58}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Otimiza\IeC {\c c}\IeC {\~a}o}{59}{subsection.5.2.3}
\contentsline {subsubsection}{Produto Interno de duas matrizes com transposi\IeC {\c c}\IeC {\~a}o de uma}{60}{section*.83}
\contentsline {subsubsection}{Mesma opera\IeC {\c c}\IeC {\~a}o algebrica}{61}{section*.85}
\contentsline {subsection}{\numberline {5.2.4}Avalia\IeC {\c c}\IeC {\~a}o da AST}{61}{subsection.5.2.4}
\contentsline {chapter}{\numberline {6}AI-ML}{65}{chapter.6}
\contentsline {section}{\numberline {6.1}Nota\IeC {\c c}\IeC {\~o}es}{65}{section.6.1}
\contentsline {subsubsection}{Hip\IeC {\'o}tese}{65}{section*.87}
\contentsline {subsubsection}{Fun\IeC {\c c}\IeC {\~a}o de custo}{65}{section*.88}
\contentsline {subsubsection}{Fun\IeC {\c c}\IeC {\~a}o de custo}{65}{section*.89}
\contentsline {subsubsection}{Gradiente Descendente}{65}{section*.90}
\contentsline {section}{\numberline {6.2}Regress\IeC {\~a}o Linear}{66}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Hip\IeC {\'o}tese}{66}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Fun\IeC {\c c}\IeC {\~a}o de Custo}{68}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Gradiente Descendente}{68}{subsection.6.2.3}
\contentsline {subsection}{\numberline {6.2.4}Equa\IeC {\c c}\IeC {\~a}o normal}{69}{subsection.6.2.4}
\contentsline {subsubsection}{Equa\IeC {\c c}\IeC {\~a}o Normal vs Gradiente Descendente}{69}{section*.93}
\contentsline {subsection}{\numberline {6.2.5}Treino e avalia\IeC {\c c}\IeC {\~a}o}{69}{subsection.6.2.5}
\contentsline {subsubsection}{Exemplo 1: Equa\IeC {\c c}\IeC {\~a}o Normal}{70}{section*.94}
\contentsline {subsubsection}{Exemplo 2: Gradiente Descendente}{70}{section*.96}
\contentsline {subsubsection}{Exemplo 3: Gradiente Descendente com Regulariza\IeC {\c c}\IeC {\~a}o}{71}{section*.99}
\contentsline {section}{\numberline {6.3}Regress\IeC {\~a}o Log\IeC {\'\i }stica}{72}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Fun\IeC {\c c}\IeC {\~a}o Hip\IeC {\'o}tese}{72}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Fun\IeC {\c c}\IeC {\~a}o de Custo}{74}{subsection.6.3.2}
\contentsline {subsection}{\numberline {6.3.3}Gradiente Descendente}{75}{subsection.6.3.3}
\contentsline {subsection}{\numberline {6.3.4}Treino e avalia\IeC {\c c}\IeC {\~a}o}{76}{subsection.6.3.4}
\contentsline {subsubsection}{Accuracy}{77}{section*.106}
\contentsline {subsubsection}{Precision}{77}{section*.107}
\contentsline {subsubsection}{Recall}{78}{section*.108}
\contentsline {subsubsection}{F1}{78}{section*.109}
\contentsline {subsubsection}{Treino da Regress\IeC {\~a}o Log\IeC {\'\i }stica}{78}{section*.110}
\contentsline {subsubsection}{Treino da Regress\IeC {\~a}o Log\IeC {\'\i }stica com Regulariza\IeC {\c c}\IeC {\~a}o}{78}{section*.112}
\contentsline {section}{\numberline {6.4}Redes Neuronais}{79}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Modelo de uma Rede Neuronal}{79}{subsection.6.4.1}
\contentsline {subsection}{\numberline {6.4.2}Unidades Log\IeC {\'\i }sticas}{80}{subsection.6.4.2}
\contentsline {subsection}{\numberline {6.4.3}Fun\IeC {\c c}\IeC {\~o}es de Ativa\IeC {\c c}\IeC {\~a}o}{81}{subsection.6.4.3}
\contentsline {subsubsection}{Sigmoid}{81}{section*.117}
\contentsline {subsubsection}{ReLU}{82}{section*.118}
\contentsline {subsubsection}{Leaky ReLU}{82}{section*.119}
\contentsline {subsubsection}{Tanh}{83}{section*.120}
\contentsline {subsubsection}{M\IeC {\'u}ltiplas Unidades de Sa\IeC {\'\i }da}{83}{section*.121}
\contentsline {subsection}{\numberline {6.4.4}Propaga\IeC {\c c}\IeC {\~a}o para a frente}{84}{subsection.6.4.4}
\contentsline {subsubsection}{Inicializa\IeC {\c c}\IeC {\~a}o correta das matrizes}{86}{section*.123}
\contentsline {subsection}{\numberline {6.4.5}Fun\IeC {\c c}\IeC {\~a}o de Custo}{86}{subsection.6.4.5}
\contentsline {subsection}{\numberline {6.4.6}Propaga\IeC {\c c}\IeC {\~a}o para tr\IeC {\'a}s}{87}{subsection.6.4.6}
\contentsline {subsection}{\numberline {6.4.7}Treino e avalia\IeC {\c c}\IeC {\~a}o}{88}{subsection.6.4.7}
\contentsline {chapter}{\numberline {7}Conclus\IeC {\~a}o}{91}{chapter.7}
\contentsline {chapter}{Bibliografia}{93}{chapter*.124}
